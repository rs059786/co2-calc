import Co2Emission from '../../src/service/Co2Emission';

describe('Co2Emission', () => {
    let co2Emission: Co2Emission;
    describe('co2Calculator', () => {
        test('successfully return co2 emisssion in kg when unit of distance in km', () => {
            const co2Emission = new Co2Emission('medium-diesel-car', 15, 'km');
            const result = co2Emission.co2Calculator();
            expect(result).toEqual('2.6kg');
        });
        test('default value for unit is km and in that case output should return in kg', () => {
            const co2Emission = new Co2Emission('large-petrol-car', 1800.5);
            const result = co2Emission.co2Calculator();
            expect(result).toEqual('507.7kg');
        });
        test('successfully return co2 emisssion in gram when unit of distance in meter', () => {
            const co2Emission = new Co2Emission('train', 14500, 'm');
            const result = co2Emission.co2Calculator();
            expect(result).toEqual('87g');
        });
        test('if unit of output is provided as argument then result should return in output unit type', () => {
            const co2Emission = new Co2Emission('train', 14500, 'm', 'kg');
            const result = co2Emission.co2Calculator();
            expect(result).toEqual('0.1kg');
        });
    });
});