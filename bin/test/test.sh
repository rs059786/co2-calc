#!/usr/bin/env bash

echo "--------testing started--------"
node ./node_modules/jest/bin/jest --config=./jest.config.js --logHeapUsage --maxWorker=8 $@