#!/usr/bin/env node
import * as yargs from 'yargs';
// import Co2Emission from './service/Co2Emission';
// import { allTransportationType, outputUnitType, unitOfDistanceType } from './service/TransportationDataInterface';
// import { checkTransportMethod, checkUnitOfDistance, checkOutputUnit } from './utils/TransportUtils';

const args = yargs.options({
  'transportation-method': { type: 'string', demandOption: true, alias: 't' },
  'distance': { type: 'number', demandOption: true, alias: 'd' },
  'unit-of-distance': { type: 'string', alias: 'u' },
  'output': { type: 'string', alias: 'o' },
}).argv;

console.log('hello world', args["transportation-method"], args.distance, args["unit-of-distance"], args["output"]);

// let transportationMethod: allTransportationType | undefined;

// transportationMethod = checkTransportMethod(args["transportation-method"]);

// let unitOfDistance: unitOfDistanceType | undefined;

// if (args["unit-of-distance"]) {
//     unitOfDistance = checkUnitOfDistance(args["unit-of-distance"]);
//     if (unitOfDistance === undefined) {
//         console.log('invalid unit of distance');
//         process.exit(2);
//     }
// }

// let outputUnit: outputUnitType | undefined;
// if (args.output) {
//     outputUnit = checkOutputUnit(args.output);
//     if (outputUnit === undefined) {
//         console.log('invalid output');
//         process.exit(2);
//     }
// }
// const distance = args.distance;

// if (transportationMethod === undefined) {
//     console.log(`invalid transportMethod`);
//     process.exit(2);
// }
// const co2Emission = new Co2Emission(transportationMethod, distance, unitOfDistance, outputUnit);