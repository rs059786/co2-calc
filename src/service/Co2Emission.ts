import { allTransportWithCo2Emission, allTransportationType, unitOfDistanceType, outputUnitType } from './TransportationDataInterface';

export default class Co2Emission {
    private transportationMethod: allTransportationType;
    private distance: number;
    private unitOfDistance: unitOfDistanceType = 'km';
    private outputUnitType?: outputUnitType;
    constructor(transportationMethod: allTransportationType, distance: number, unitOfDistance?: unitOfDistanceType, outputType?: outputUnitType) {
        this.transportationMethod = transportationMethod;
        this.distance = distance;
        if (unitOfDistance) {
            this.unitOfDistance = unitOfDistance;
        }
        this.outputUnitType = outputType ? outputType : undefined;
    }

    public co2Calculator(): string {
        let transportCo2Emission = parseFloat(allTransportWithCo2Emission[this.transportationMethod]);
        if (this.unitOfDistance === 'm') {
            transportCo2Emission = transportCo2Emission / 1000; // converts transport emission per m
        }
        const distanceCovered = this.distance;
        let totalCo2Emit = transportCo2Emission * distanceCovered;
        if (this.unitOfDistance === 'km' && (!this.outputUnitType || this.outputUnitType !== 'g')) {
            totalCo2Emit = totalCo2Emit / 1000; // converts emission in kg as unit of distance in km
        }
        if (this.unitOfDistance === 'm' && this.outputUnitType === 'kg') {
            totalCo2Emit = totalCo2Emit / 1000; // convert emission in kg
        }
        totalCo2Emit = Math.round((totalCo2Emit + Number.EPSILON) * 10) / 10;
        const getCo2EmissionUnitType = this.getCo2EmissionUnitType(this.unitOfDistance, this.outputUnitType);
        return `${totalCo2Emit}${getCo2EmissionUnitType}`;
    }

    public getCo2EmissionUnitType (unitOfDistance: unitOfDistanceType, outputUnitType?: outputUnitType): outputUnitType{
        if (outputUnitType) {
            return outputUnitType;
        } else {
            if (unitOfDistance === 'km') {
                return 'kg';
            } else {
                return 'g';
            }
        }
    }
}